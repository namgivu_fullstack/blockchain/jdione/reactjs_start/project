import Image from 'next/image';

export default function Home() {
  return (
   <div className='dau pt-12 text-zinc-950 '>
      <h1 className='text-zinc-950 text-center text-5xl font-medium mb-10'> bao tuoi tre</h1>

      <p>Thủ tướng Phạm Minh Chính và phu nhân đến điểm hẹn trước để đón hai vị khách quý. Khi chiếc xe chở Thủ tướng Malaysia Anwar Ibrahim vừa đến, Thủ tướng Phạm Minh Chính đã ra tận cửa xe đón tiếp niềm nở.
Nhận ra sự giản dị của Thủ tướng Việt Nam, Thủ tướng Malaysia đã nhanh chóng cởi bỏ chiếc cà vạt và để áo ngoài quần. Hai nhà lãnh đạo cùng phu nhân sau đó tham quan một số hiệu sách tại Đường sách 19-12, trao đổi về những vấn đề cùng quan tâm bên cạnh những quyển sách.</p>

      <div className='grid grid-cols-3 px-8 py-8 space-x-10 mx-auto'>
        <div>
          <div className='h-96'>
            <img src="https://i.pinimg.com/564x/ba/0f/49/ba0f49642b6545eb2531de2460513550.jpg" alt="" 
            className='w-full h-full rounded-lg object-cover' />
          </div>

          <h2 className='text-center font-medium text-lg'> huyen ao</h2>
          <span>6,789 proteti </span>
        </div>
        <div>
          <div className='h-96'>
            <img src="https://i.pinimg.com/564x/16/ef/e0/16efe0bd86859e8e51bc9d648ff62f95.jpg" alt="" 
            className='w-full h-full rounded-lg object-cover' />
          </div>

          <h2 className='text-center font-medium text-lg'> huyen ao</h2>
          <span>6,789 proteti </span>
        </div>
        <div>
          <div className='h-96'>
            <img src="https://i.pinimg.com/564x/91/c5/5b/91c55b6dc879d17e3e69f199ffc02369.jpg" alt="" 
            className='w-full h-full rounded-lg object-cover' />
          </div>

          <h2 className='text-center font-medium text-lg'> huyen ao</h2>
          <span>6,789 proteti </span>
        </div>
      </div>
      <div className='flex justify-center mt-10'> {/* Sử dụng lớp CSS flex và justify-center để căn chỉnh bảng vào giữa trang */}
        <div>
          <table className='table-auto'>
            <caption className="table-caption">
            </caption>
            <thead>
              <tr>
                <th>Wrestler</th>
                <th>Signature Move(s)</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>"Stone Cold" Steve Austin</td>
                <td>Stone Cold Stunner, Lou Thesz Press</td>
              </tr>
              <tr>
                <td>Bret "The Hitman" Hart</td>
                <td>The Sharpshooter</td>
              </tr>
              <tr>
                <td>Razor Ramon</td>
                <td>Razor's Edge, Fallaway Slam</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      
    </div>
  )
}
